package com.example.myapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ProductAddNewActivity extends AppCompatActivity {

    DrawerLayout drawerLayout;
    FirebaseDatabase rootNode;
    DatabaseReference reference;

    EditText productName, productSku, productColor, productSizes;
    AppCompatSpinner productCategory;
    Button addProduct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_add_new);

        drawerLayout = findViewById(R.id.drawer_layout);

        final String[] category = new String[1];

        productName = (EditText) findViewById(R.id.productName);
        productSku = (EditText) findViewById(R.id.productSku);
        productColor = (EditText) findViewById(R.id.productColor);
        productSizes = (EditText) findViewById(R.id.productSizes);

        productCategory = (AppCompatSpinner) findViewById(R.id.productCategory);

        ArrayAdapter<CharSequence> adapter;
        adapter = ArrayAdapter.createFromResource(this, R.array.categories, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        productCategory.setAdapter(adapter);

        addProduct = findViewById(R.id.buttonAddProduct);
        addProduct.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                rootNode = FirebaseDatabase.getInstance();
                reference = rootNode.getReference("Products");

                String name = productName.getText().toString();
                String sku = productSku.getText().toString();
                String color = productColor.getText().toString();
                String size = productSizes.getText().toString();
                String category = productCategory.getSelectedItem().toString();

                Product product = new Product(name, sku, category, color, size);

                // walidacja
                if (!validateProductName() | !validateColor() | !validateSizes() | !validateSku() | !validateSkuLength()){
                    return;
                }

                reference.child(sku).setValue(product);

                Toast.makeText(ProductAddNewActivity.this, "Pomyślnie dodano nowy produkt!", Toast.LENGTH_SHORT).show();
                clearForm();
            }
        });

    }

    public void clearForm() {
        productName.setText("");
        productSku.setText("");
        productColor.setText("");
        productSizes.setText("");
        productCategory.setSelection(0);
    }

    public void ClickMenu(View view) {
        openDrawer(drawerLayout);
    }

    private static void openDrawer(DrawerLayout drawerLayout) {
        drawerLayout.openDrawer(GravityCompat.START);
    }

    public void ClickMenuText(View view) {
        redirectActivity(this, MenuActivity.class);
    }

    private static void closeDrawer(DrawerLayout drawerLayout) {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    public void ClickAllProducts(View view) {
        redirectActivity(this, ProductViewListActivity.class);
    }

    public void ClickAddProduct(View view) {

        recreate();
    }

    public void ClickRemoveProduct(View view) {
        redirectActivity(this, ProductRemoveActivity.class);
    }

    public void ClickEditProduct(View view) {
        redirectActivity(this, ProductEditActivity.class);
    }

    public void ClickLogout(View view) {
        logout(this);
    }

    public void logout(Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Logout");
        builder.setMessage("Are you sure you want to logout?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(ProductAddNewActivity.this, MainActivity.class);
                ProductAddNewActivity.this.startActivity(intent);
                finish();
//                redirectActivity(this, ProductListViewActivity.class);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public static void redirectActivity(Activity activity, Class aClass) {
        Intent intent = new Intent(activity, aClass);
        intent.setFlags((Intent.FLAG_ACTIVITY_NEW_TASK));
        activity.startActivity(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        closeDrawer(drawerLayout);
    }

    private boolean validateProductName() {
        String val = productName.getText().toString().trim();

        if (val.isEmpty()) {
            productName.setError("This field can not be empty!");
            return false;
        } else {
            productName.setError(null);
            return true;
        }
    }

    private boolean validateSkuLength() {
        String val = productSku.getText().toString().trim();

        if (val.length() < 4) {
            productSku.setError("Sku must have 4 digits!");
            return false;
        }
        else {
            productSku.setError(null);
            return true;
        }
    }

    private boolean validateSku() {
        String val = productSku.getText().toString().trim();

        if (val.isEmpty()) {
            productSku.setError("This field can not be empty!");
            return false;
        } else {
            productSku.setError(null);
            return true;
        }
    }

    private boolean validateColor() {
        String val = productColor.getText().toString().trim();

        if (val.isEmpty()) {
            productColor.setError("This field can not be empty!");
            return false;
        } else {
            productColor.setError(null);
            return true;
        }
    }

    private boolean validateSizes() {
        String val = productSizes.getText().toString().trim();

        if (val.isEmpty()) {
            productSizes.setError("This field can not be empty!");
            return false;
        } else {
            productSizes.setError(null);
            return true;
        }
    }

}
