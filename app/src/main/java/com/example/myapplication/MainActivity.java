package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText loginEditText, passwordEditText;
    Button loginBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loginEditText = (EditText) findViewById(R.id.loginName);
        passwordEditText = (EditText) findViewById(R.id.password);

        loginBtn = findViewById(R.id.loginButton);
        loginBtn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                String logintext = loginEditText.getText().toString();
                String passwordtext = passwordEditText.getText().toString();

                boolean cos = isUserAssigned(logintext, passwordtext);
                if (cos) {
                    Intent intent = new Intent(MainActivity.this, MenuActivity.class);
                    MainActivity.this.startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(MainActivity.this, "User not found!", Toast.LENGTH_SHORT).show();
                }

            }

            public boolean isUserAssigned(String log, String pass) {
                // TODO dopisac sprawdzenie loginu i hasla w bazie danych
                String logDB = "admin";
                String passDB = "admin";

                if (log.equals(logDB) && pass.equals(passDB)) {
                    return true;
                }
                return false;
            }
        });

    }
}