package com.example.myapplication;

        import android.app.Activity;
        import android.app.AlertDialog;
        import android.content.DialogInterface;
        import android.content.Intent;
        import android.os.Bundle;
        import android.util.Log;
        import android.view.View;
        import android.widget.AdapterView;
        import android.widget.ArrayAdapter;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.Toast;

        import androidx.annotation.NonNull;
        import androidx.appcompat.app.AppCompatActivity;
        import androidx.appcompat.widget.AppCompatSpinner;
        import androidx.core.view.GravityCompat;
        import androidx.drawerlayout.widget.DrawerLayout;

        import com.google.android.gms.common.util.ArrayUtils;
        import com.google.firebase.database.DataSnapshot;
        import com.google.firebase.database.DatabaseError;
        import com.google.firebase.database.DatabaseReference;
        import com.google.firebase.database.FirebaseDatabase;
        import com.google.firebase.database.ValueEventListener;

        import java.util.ArrayList;

public class ProductEditActivity extends AppCompatActivity {

    DrawerLayout drawerLayout;
    FirebaseDatabase rootNode;
    DatabaseReference reference;

    EditText productNameEdit, productSkuEdit, productColorEdit, productSizesEdit;
    AppCompatSpinner productCategoryEdit, productChosenEdit;
    Button editProduct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_edit);

        drawerLayout = findViewById(R.id.drawer_layout);

        productNameEdit = (EditText) findViewById(R.id.productNameEdit);
        productSkuEdit = (EditText) findViewById(R.id.productSkuEdit);
        productSkuEdit.setEnabled(false);
        productColorEdit = (EditText) findViewById(R.id.productColorEdit);
        productSizesEdit = (EditText) findViewById(R.id.productSizesEdit);
        productCategoryEdit = (AppCompatSpinner) findViewById(R.id.productCategoryEdit);

        ArrayAdapter<CharSequence> adapter;
        adapter = ArrayAdapter.createFromResource(this, R.array.categories, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        productCategoryEdit.setAdapter(adapter);


        ArrayList<Product> allProducts = new ArrayList();
        ArrayList<String> chooseList = new ArrayList();

        rootNode = FirebaseDatabase.getInstance();
        reference = rootNode.getReference("Products");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    Product prod = new Product(
                            postSnapshot.child("productName").getValue().toString(),
                            postSnapshot.child("sku").getValue().toString(),
                            postSnapshot.child("category").getValue().toString(),
                            postSnapshot.child("color").getValue().toString(),
                            postSnapshot.child("sizes").getValue().toString());
                    allProducts.add(prod);
                    chooseList.add(prod.sku + " | " + prod.productName);
                    productChosenEdit = (AppCompatSpinner) findViewById(R.id.productChosenEdit);
                    String[] stringChooseList = chooseList.toArray(new String[chooseList.size()]);
                    ArrayAdapter<String> adapter;
                    adapter = new ArrayAdapter<String>(ProductEditActivity.this, android.R.layout.simple_spinner_item, stringChooseList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    productChosenEdit.setAdapter(adapter);

                    productChosenEdit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            Product tmp = new Product();
                            for (int i = 0; i < allProducts.size(); i++){
                                if (productChosenEdit.getSelectedItem().toString().substring(0, 4).equals(allProducts.get(i).getSku())){
//                                    Toast.makeText(ProductEditActivity.this, getResources().getStringArray(R.array.categories).toString().indexOf(allProducts.get(i).getCategory()), Toast.LENGTH_SHORT).show();
//                                    Log.d("indeks: ", String.valueOf(getResources().getStringArray(R.array.categories).toString().indexOf(allProducts.get(i).getCategory())));
//                                    Log.d("katy:", getResources().getStringArray(R.array.categories).asList(R.array.categories).indexOf("Sedan"););
//                                    Log.d("tu:" ,ArrayUtils.indexOf(getResources().getStringArray(R.array.categories), "Koszule"));
                                    int index = 0;
                                    for (int j = 0; j < getResources().getStringArray(R.array.categories).length; j ++){
                                        if (getResources().getStringArray(R.array.categories)[j].equals(allProducts.get(i).getCategory())){
                                            index = j;
                                        }
                                    }
                                    Log.d("indeks po: ", String.valueOf(index));
                                    productNameEdit.setText(allProducts.get(i).getProductName());
                                    productSkuEdit.setText(allProducts.get(i).getSku());
                                    productCategoryEdit.setSelection(index);
                                    productColorEdit.setText(allProducts.get(i).getColor());
                                    productSizesEdit.setText(allProducts.get(i).getSizes());
                                }
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(ProductEditActivity.this, "Database error", Toast.LENGTH_SHORT).show();
            }
        });



        editProduct = findViewById(R.id.buttonEditProduct);
        editProduct.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String skuToEdit = productChosenEdit.getSelectedItem().toString().substring(0, 4);

                String name = productNameEdit.getText().toString();
                String sku = productSkuEdit.getText().toString();
                String color = productColorEdit.getText().toString();
                String size = productSizesEdit.getText().toString();
                String category = productCategoryEdit.getSelectedItem().toString();

                Product productEdit = new Product(name, sku, category, color, size);

                // walidacja
                if (!validateProductName() | !validateColor() | !validateSizes()){
                    return;
                }

                reference.child(skuToEdit).setValue(productEdit);

                Toast.makeText(ProductEditActivity.this, "Pomyślnie zedytowano wybrany produkt!", Toast.LENGTH_SHORT).show();
                chooseList.clear();
            }
        });


    }

    public void ClickMenu (View view) {
        openDrawer(drawerLayout);
    }

    private static void openDrawer(DrawerLayout drawerLayout) {
        drawerLayout.openDrawer(GravityCompat.START);
    }

    public void ClickMenuText (View view) {
        redirectActivity(this, MenuActivity.class);
    }

    private static void closeDrawer(DrawerLayout drawerLayout) {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    public void ClickAllProducts(View view) {
        redirectActivity(this, ProductViewListActivity.class);
    }

    public void ClickAddProduct(View view){
        redirectActivity(this, ProductAddNewActivity.class);
    }

    public void ClickRemoveProduct(View view){
        redirectActivity(this, ProductRemoveActivity.class);
    }

    public void ClickEditProduct(View view){
        recreate();
    }

    public void ClickLogout(View view){
        logout(this);
    }

    public void logout(Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Logout");
        builder.setMessage("Are you sure you want to logout?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(ProductEditActivity.this, MainActivity.class);
                ProductEditActivity.this.startActivity(intent);
                finish();
//                redirectActivity(this, ProductListViewActivity.class);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public static void redirectActivity(Activity activity, Class aClass) {
        Intent intent = new Intent(activity, aClass);
        intent.setFlags((Intent.FLAG_ACTIVITY_NEW_TASK));
        activity.startActivity(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        closeDrawer(drawerLayout);
    }

    private boolean validateProductName() {
        String val = productNameEdit.getText().toString().trim();

        if (val.isEmpty()) {
            productNameEdit.setError("This field can not be empty!");
            return false;
        } else {
            productNameEdit.setError(null);
            return true;
        }
    }

    private boolean validateColor() {
        String val = productColorEdit.getText().toString().trim();

        if (val.isEmpty()) {
            productColorEdit.setError("This field can not be empty!");
            return false;
        } else {
            productColorEdit.setError(null);
            return true;
        }
    }

    private boolean validateSizes() {
        String val = productSizesEdit.getText().toString().trim();

        if (val.isEmpty()) {
            productSizesEdit.setError("This field can not be empty!");
            return false;
        } else {
            productSizesEdit.setError(null);
            return true;
        }
    }


}
