package com.example.myapplication;

public class Product {
    String productName, sku, category, color, sizes;

    public Product(String productName, String sku, String category, String color, String sizes) {
        this.productName = productName;
        this.sku = sku;
        this.category = category;
        this.color = color;
        this.sizes = sizes;
    }

    public Product() {
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSizes() {
        return sizes;
    }

    public void setSizes(String sizes) {
        this.sizes = sizes;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productName='" + productName + '\'' +
                ", sku='" + sku + '\'' +
                ", category='" + category + '\'' +
                ", color='" + color + '\'' +
                ", sizes='" + sizes + '\'' +
                '}';
    }
}
