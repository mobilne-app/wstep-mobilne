package com.example.myapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ProductRemoveActivity extends AppCompatActivity {

    DrawerLayout drawerLayout;
    FirebaseDatabase rootNode;
    DatabaseReference reference;

    AppCompatSpinner productChosen;
    Button removeProduct;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_remove);

        drawerLayout = findViewById(R.id.drawer_layout);

        ArrayList<Product> allProducts = new ArrayList();
        ArrayList<String> chooseList = new ArrayList();

        rootNode = FirebaseDatabase.getInstance();
        reference = rootNode.getReference("Products");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    Product prod = new Product(
                            postSnapshot.child("productName").getValue().toString(),
                            postSnapshot.child("sku").getValue().toString(),
                            postSnapshot.child("category").getValue().toString(),
                            postSnapshot.child("color").getValue().toString(),
                            postSnapshot.child("sizes").getValue().toString());
                    allProducts.add(prod);
                    chooseList.add(prod.sku + " | " + prod.productName);
                    productChosen = (AppCompatSpinner) findViewById(R.id.productChosen);
                    String[] stringChooseList = chooseList.toArray(new String[chooseList.size()]);
                    ArrayAdapter<String> adapter;
                    adapter = new ArrayAdapter<String>(ProductRemoveActivity.this, android.R.layout.simple_spinner_item, stringChooseList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    productChosen.setAdapter(adapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(ProductRemoveActivity.this, "Database error", Toast.LENGTH_SHORT).show();
            }
        });

        removeProduct = findViewById(R.id.buttonRemoveProduct);
        removeProduct.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String skuToRemove = productChosen.getSelectedItem().toString().substring(0, 4);
                reference.child(skuToRemove).removeValue();
                Toast.makeText(ProductRemoveActivity.this, "Pomyślnie usunięto wybrany produkt!", Toast.LENGTH_SHORT).show();
                chooseList.clear();
            }
        });
    }

    public void ClickMenu(View view) {
        openDrawer(drawerLayout);
    }

    private static void openDrawer(DrawerLayout drawerLayout) {
        drawerLayout.openDrawer(GravityCompat.START);
    }

    public void ClickMenuText(View view) {
        redirectActivity(this, MenuActivity.class);
    }

    private static void closeDrawer(DrawerLayout drawerLayout) {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    public void ClickAllProducts(View view) {
        redirectActivity(this, ProductViewListActivity.class);
    }

    public void ClickAddProduct(View view) {
        redirectActivity(this, ProductAddNewActivity.class);
    }

    public void ClickRemoveProduct(View view) {
        recreate();
    }

    public void ClickEditProduct(View view) {
        redirectActivity(this, ProductEditActivity.class);
    }

    public void ClickLogout(View view) {
        logout(this);
    }

    public void logout(Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Logout");
        builder.setMessage("Are you sure you want to logout?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(ProductRemoveActivity.this, MainActivity.class);
                ProductRemoveActivity.this.startActivity(intent);
                finish();
//                redirectActivity(this, ProductListViewActivity.class);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public static void redirectActivity(Activity activity, Class aClass) {
        Intent intent = new Intent(activity, aClass);
        intent.setFlags((Intent.FLAG_ACTIVITY_NEW_TASK));
        activity.startActivity(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        closeDrawer(drawerLayout);
    }

}

