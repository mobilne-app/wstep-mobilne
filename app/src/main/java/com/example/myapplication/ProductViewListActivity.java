package com.example.myapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class ProductViewListActivity extends AppCompatActivity {

    DrawerLayout drawerLayout;
    FirebaseDatabase rootNode;
    DatabaseReference reference;

    ListView listView;

//    Button testButton;

    Dialog myDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_view_list);

        drawerLayout = findViewById(R.id.drawer_layout);

//        testButton = findViewById(R.id.buttonIncrease);

        myDialog = new Dialog(this);

        listView = findViewById(R.id.productsList);

        ArrayList<Product> allProducts = new ArrayList();

        rootNode = FirebaseDatabase.getInstance();
        reference = rootNode.getReference("Products");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    Product prod = new Product(
                            postSnapshot.child("productName").getValue().toString(),
                            postSnapshot.child("sku").getValue().toString(),
                            postSnapshot.child("category").getValue().toString(),
                            postSnapshot.child("color").getValue().toString(),
                            postSnapshot.child("sizes").getValue().toString());
                    allProducts.add(prod);

                    ArrayList<String> listToDisplay = new ArrayList<>();
                    for (Product p : allProducts) {
                        listToDisplay.add(p.getSku() + " | " + p.getProductName());
                    }

                    ArrayAdapter adapter = new ArrayAdapter(ProductViewListActivity.this, android.R.layout.simple_list_item_1, listToDisplay);

                    listView.setAdapter(adapter);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Log.d("pozycja", String.valueOf(position));

                            showPopup(view, allProducts.get(position));
                        }
                    });

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(ProductViewListActivity.this, "Database error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void ClickMenu(View view) {
        openDrawer(drawerLayout);
    }

    private static void openDrawer(DrawerLayout drawerLayout) {
        drawerLayout.openDrawer(GravityCompat.START);
    }

    public void ClickMenuText(View view) {
        redirectActivity(this, MenuActivity.class);
    }

    private static void closeDrawer(DrawerLayout drawerLayout) {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    public void ClickAllProducts(View view) {
        recreate();
    }

    public void ClickAddProduct(View view) {
        redirectActivity(this, ProductAddNewActivity.class);
    }

    public void ClickRemoveProduct(View view) {
        redirectActivity(this, ProductRemoveActivity.class);
    }

    public void ClickEditProduct(View view) {
        redirectActivity(this, ProductEditActivity.class);
    }

    public void ClickLogout(View view) {
        logout(this);
    }

    public void logout(Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Logout");
        builder.setMessage("Are you sure you want to logout?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(ProductViewListActivity.this, MainActivity.class);
                ProductViewListActivity.this.startActivity(intent);
                finish();
//                redirectActivity(this, ProductListViewActivity.class);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public static void redirectActivity(Activity activity, Class aClass) {
        Intent intent = new Intent(activity, aClass);
        intent.setFlags((Intent.FLAG_ACTIVITY_NEW_TASK));
        activity.startActivity(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        closeDrawer(drawerLayout);
    }


    public void showPopup(View v, Product p) {
        Log.d("produkt", p.toString());
        TextView txtClose, productName, productSku, productCategory, productColor, productSize;
        myDialog.setContentView(R.layout.activity_product_popup);
        txtClose = (TextView) myDialog.findViewById(R.id.closePopup);
        productName = (TextView) myDialog.findViewById(R.id.productNamePopup);
        productSku = (TextView) myDialog.findViewById(R.id.productSkuPopup);
        productCategory = (TextView) myDialog.findViewById(R.id.productCategoryPopup);
        productColor = (TextView) myDialog.findViewById(R.id.productColorPopup);
        productSize = (TextView) myDialog.findViewById(R.id.productSizesPopup);

        productName.setText(p.getProductName());
        productSku.setText(p.getSku());
        productCategory.setText(p.getCategory());
        productColor.setText(p.getColor());
        productSize.setText(p.getSizes());

        txtClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDialog.dismiss();
            }
        });
        myDialog.show();
    }

}
